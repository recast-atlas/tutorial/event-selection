// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// Plotting
#include "TCanvas.h"
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
// our library functionality
#include "JetSelectionHelper/JetSelectionHelper.h"

int main(int argc, char** argv) {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "mc16_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r10724_p3840/DAOD_EXOT27.17882736._000019.pool.root.1";
  if(argc >= 2) inputFilePath = argv[1];

  TString outputFilePath = "myOutputFile.root";
  if(argc >=3) outputFilePath = argv[2];

  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // get the number of events in the file to loop over
  Long64_t numEntries = -1;
  if(argc >= 4) numEntries = std::atoi(argv[3]);
  if(numEntries == -1) numEntries = event.getEntries();

  std::cout << "Processing " << numEntries << " events" << std::endl;

  // make our jet selector tool
  JetSelectionHelper myJetTool;

  // make histograms for storage
  TH1D *h_njets = new TH1D("h_njets","Number of Jets",20,0,20);
  TH1D *h_mjj = new TH1D("h_mjj","Dijet Invariant Mass [GeV]",20,0,500);

  // for counting events
  unsigned count = 0;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    float mcEventWeight = ei->mcEventWeights().at(0);
    if(i%10000==0) std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << ", MC event weight: " << mcEventWeight << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* akt4_jets = nullptr;
    event.retrieve(akt4_jets, "AntiKt4EMTopoJets");

    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> signal_jets;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *akt4_jets) {
      // print the kinematics of each jet in the event
      if(i%10000==0) std::cout << "Jet : " << jet->pt() << "\t" << jet->eta() << "\t" << jet->phi() << "\t" << jet->m() << std::endl;

      if( myJetTool.isJetGood(jet) ){
        signal_jets.push_back(*jet);
      }
    }

    // fill the analysis histograms accordingly
    h_njets->Fill( signal_jets.size(), mcEventWeight );

    if( signal_jets.size()>=2 ){
      h_mjj->Fill( (signal_jets.at(0).p4()+signal_jets.at(1).p4()).M()/1000., mcEventWeight );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // open TFile to store the analysis histogram output
  TFile *fout = new TFile(outputFilePath, "RECREATE");

  h_njets->Write();
  h_mjj->Write();

  fout->Close();

  //Create a canvas
  TCanvas* TCan = new TCanvas("TCan", "TCan", 40, 40, 800, 500);
  h_njets->Draw();
  TCan->Print("njets.pdf");
  h_mjj->Draw();
  TCan->Print("mjj.pdf");

  // exit from the main function cleanly
  return 0;
}
