# Event Selection
This is the starting point for the toy analysis.  This is just an event loop
that does a basic event selection and plots the invariant mass of two b jets,
presuming a search for a di-bjet resonance or something.

# Getting the Package

Note that in this case, you should [clone the repository recursively](https://riptutorial.com/git/example/11913/clone-recursively)
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/recast-examples/event-selection.git
```
(NOTE : The precise repository may not be the same if the user is using their own space.)

# Compilation

In either case, the user should start by pulling the ATLAS `AnalysisBase:21.2.85-centos7` docker image
and entering into the docker image as below
```
docker run --rm -it -v $PWD:/home/atlas/Bootcamp atlas/analysisbase:21.2.85-centos7 bash
```
which will place the entire file structure within the `/home/atlas/Bootcamp` directory
of the image.  Go one level above the directory of the repository and create a build directory.
In this directory, perform the CMake configuration and compilation
```
cd ../event-selection
source ~/release_setup.sh
mkdir build
cd build
cmake ../source
make
```
which will produce a locally executable `AnalysisPayload` again.

# Output

The output file contains histograms with the number of jets and the dijet invariant mass.
This also produces a couple PDF files of these plots for validation.

# Solution

If you are wondering how to build the Dockerfile, looking in the `final` branch.
